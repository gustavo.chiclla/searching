const { readdirSync, readFileSync } = require('fs')
const { glob } = require("glob");
const lineByLine = require("n-readlines")


//word to find
const listWords = ["servicing.13000004.ldap.delUser","ldap.delUser"]


//Exceptions
const configuration = {
    exceptDir: ["rm", "master", "develop", "pzic"],
    // filesScanFormat: "properties|java"
    filesScanFormat: "properties|java"
}

//url path
let path = "D:/Entelgy/PROYECTOS/"

//Get all directories 
const getDirectories = source =>
    readdirSync(source, { withFileTypes: true })
        .filter(dirent => dirent.isDirectory())
        .map(dirent => dirent.name)


//compare with exception files
const filterNames = (fileName) => {
    let temp = "";
    configuration.exceptDir.forEach(x => {
        if (fileName.includes(x)) {
            temp = fileName;
        }
    })

    if (fileName != temp) {
        return fileName
    }
}

//Filter directories root
const getDirectoriesFilter = async (url) => getDirectories(url).filter(filterNames)


//Filter dubDirectories
const getSubDirectoriesFilter = async (rootDirectories, absolutePath) => {

    let dirs = {};
    try {
        rootDirectories.forEach(x => {
            let pathPattern = `${absolutePath + x}/**/*.@(${configuration.filesScanFormat})`
            let allRootFiles = glob.sync(pathPattern, {
                ignore: [
                    '**/.git/**',
                    '**/target/**',
                ]
            })
            dirs[x] = allRootFiles;
        })
        return dirs;

    } catch (error) {
        throw error
    }

}
const getPatternRegExp = () => {

/*     const getSplitWords = listWords.map(x => {
        return x.split(".")
    })
    const getWordsOnSingleLine = getSplitWords.reduce((acc, el) => acc.concat(el), []).reduce((acc, el, index) => {
        if (index >= 1) return acc + "|" + el
        return acc + el
        // if (index >=1 ) return acc+ "|" + "\\."+ el + "\\."
        // return acc+"\\."+el+"\\."
    }, "") 
   */
    const getWordsOnSingleLine = listWords.reduce((acc, el, index) => {
        if (index >= 1) return acc + "|" + el.replace(/\./g, "\\.")
        return acc + el.replace(/\./g, "\\.")
    }, "")
    return getWordsOnSingleLine;
}

const getContentFile = async (directories, subDirectories) => {

    let wordContentForFilter = getPatternRegExp();
    // const regex = new RegExp(String.raw`(${wordContentForFilter}|\.url.encrypt.service\.|\.url.encrypt\.|\.url\.|sendEmailProductClaimMapper.mapIn )`)
    const regex = new RegExp(String.raw`(${wordContentForFilter})`)
    try {

        let subDirectoriesWithMatches = {}

        directories.forEach(project => {
            let matches = []
            subDirectories[project].forEach(directory => {
                const liner = new lineByLine(directory)
                let line;
                let lineNumber = 1;
                let arrRegex
                while (line = liner.next()) {
                    if (arrRegex=regex.exec(line.toString('ascii'))) {
                        matches.push(directory + ":" + lineNumber +" \n WORD: "+arrRegex[0]  +" \n LINE: "+ line.toString('ascii')+" \n ")
                    }
                    lineNumber++
                }
            })
            subDirectoriesWithMatches[project] = matches
        })

        return subDirectoriesWithMatches

    } catch (error) {
        throw error
    }
}


//read files for directories 
const readFiles = async (directories, absolutePath) => {

    try {
        const subDirectories = await getSubDirectoriesFilter(directories, absolutePath)
        const contentFile = await getContentFile(directories, subDirectories)
        return contentFile;

    } catch (error) {
        throw error
    }
}

async function get(url) {

    const directories = await getDirectoriesFilter(url)
    const files = await readFiles(directories, url)
    return files
}
get(path)
    .then(resp => {console.log(resp)})
    .catch(err => console.log(err))